%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 7

%    clear all residule code left
clear all
clc
%INPUTS
%For better understanding of please check out the numerical example on
%https://en.wikipedia.org/wiki/Conjugate_gradient_method
%   A   =coefficient matrix for linear system (square matrix)
A=[4,1;1,3];
%   b   =right-hand side vector for linear system (column vector)
b=[1;2];
%   x   =cinitial guess for solution of linear system(olumn vector)
x=[2;1];

ex7_func(A,b,x)
	   