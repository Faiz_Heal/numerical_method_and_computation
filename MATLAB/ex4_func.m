%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 4


function  [L,U,P]= ex4_func(A)
    %Size of coefficient matrix and check if matrix is square matrix
    [size_row ,size_col] = size ( A );
    if ( size_row ~= size_col )
       error( 'Square coefficient matrix required' );
       return;
    end;
    for i = 1 : size_row - 1
        if ( A(i,i) == 0 );
           t =  min ( find ( A(i+1:size_row,i) ~= 0 ) + i );
           %    check if matrix is Invertible or not
           if ( isempty(t) )
              error ( 'Coefficient matrix is singular' );
              return;
           end;
        end;
    end;
    % Calling inbuilt matlab function
    [L,U,P] = lu(A);
    fprintf ('Lower triangular matrix L:\n');disp (L);
    fprintf ('Upper triangular matrix U:\n');disp (U);
    fprintf ('Permutaion matrix P:\n');disp (P);
end
