%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 5


%   Cholesky decomposition of a symmetric positivematrix.
%    clear all residule code left
clc
clear all
%INPUT
%coefficient matrix for linear system (square matrix)
A=[4    12  -16
   12   37  -43
   -16  -43 98];
ex5_func(A)
