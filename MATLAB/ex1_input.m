clear all
close all
clc
%INPUT
%initial guess
x = 2;
%function
f = @(x) (x^4 - x-10);
%derivative of function
df= @(x) (4*x^3-1);
ex1_function(f,df,x);