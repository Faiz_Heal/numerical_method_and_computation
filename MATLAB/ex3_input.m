%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 3

%GAUSS_ELIM   solve the linear system Ax = b using Gaussian elimination with back substitution
%    clear all residule code left
clear all
clc
%INPUT
%coefficient matrix for linear system (square matrix)
A =[-3,2,-6;5,7,-5;1,4,-2];
%right hand side vector
b = [6;6;8];
ex3_func(A,b)