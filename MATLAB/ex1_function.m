%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 1


% call ex1_function(f,df,x);

function x = ex1_function(f,df,x)

    %Default Values
    max_iter = 100; %maximum number of iteration
    TOL= 0.0000001; %Tolerance
    iter =1;        %initilize iteration
    %nothing better than a while loop
    while iter<max_iter
        %nothing better than a while loop
        x1=x-f(x)/df(x);
        if abs(x1-x)<TOL;
            fprintf('\nIteration #%d: x=%.20f', iter, x1);
            break
        else
            fprintf('\nIteration #%d: x=%.20f', iter, x);
            x=x1;
        iter=iter+1;
        end
    end
    if iter>max_iter 
        fprintf('method not converging');
    end
end
