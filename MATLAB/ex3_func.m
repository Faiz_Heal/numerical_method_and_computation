%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 3

function x = ex3_func(A,b)
    %Size of coefficient matrix and check if matrix is square matrix
    [size_row, size_col] = size ( A );
    if ( size_row ~= size_col )
       error( 'A is not a square matrix' );
       return;
    end;
    %Size of b and check if matrix is compatible with A
    nb = length ( b );
    if ( size_row ~= nb )
       error ( 'Incompatible sizes between A & b.' )
       return;
    end;
    %   x    solution vector ( Ax = b)
    x = zeros ( 1, size_row );
    %Checking if matrix is singular or not
    if ( det(A) == 0 )
        error ('coefficient matrix is singular');
        return
    end

    %    Gaussian elimination (Upper matrix)
    for i = 1 : size_row -
        for j = i+1 : size_row
            % Finding the multiplier (Note: there is negative sign in front of m)
            m = -A(j,i) / A(i,i);
            A(j,i) = 0;   %setting values to 0 in matrix
            A(j, i+1:size_row) = A(j, i+1:size_row) + m * A(i, i+1:size_row);
            b(j) = b(j) + m * b(i);
        end;
    end;
    %    back substitution
    x(size_row) = b(size_row) / A(size_row, size_row);
    for i = size_row - 1 : -1 : 1
        x(i) = ( b(i) - sum ( x(i+1:size_row) .* A(i, i+1:size_row) ) ) / A(i,i);
    end;
end
