%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 5



function y = ex5_func(A)
    %Check if the matrix is symmetric
    [size_row,size_col]=size(A);
    if size_row~=size_col,
        % raise error
        error('A is not Symmetric');
    end
    %Test for positive definiteness
    %Boolean to check for positiveness
    x=1;
    for i=1:size_row
         %Extract upper left (k x k) submatrix
         % subA=[a b
         %      c d];
        subA=A(1:i,1:i);
         %Check if the determinent of the subA is +ve
        if(det(subA)<=0);
            x=0;
            break;
        end
    end

    if x==1;
        display('Given Matrix is Positive definite and');
        disp('Cholesky decomposition of given matrix is (L):');disp(chol(A));
        disp('and transpose is:');disp((chol(A))')
    else
        display('Given Matrix is NOT positive definite');
    end
end
