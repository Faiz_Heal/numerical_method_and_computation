%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 7


function s = ex7_func(A,b,x)
     %   TOL   =convergence tolerance -> (||residual vector||)
    TOL=1.0e-10;
    %   max_iter    =Maximum number of iteration
    max_iter=100;
    %For better understanding of please check out the numerical example on
    %https://en.wikipedia.org/wiki/Conjugate_gradient_method
    n = length ( b );
    [r,c] = size ( A );
    if ( c ~= n )
       disp ( 'ERROR: matrix dimensions and vector dimension not compatible' )
       return
    end;
    %Our first step is to calculate the residual vector r associated with x.
    %This residual is computed from the formula r = Ax-b, and in our case is equal to
    r = A * x - b;
    %   r'    =transpose of r
    %for convienience we already compute r'*r as delta0
    delta0 = r' * r;
    % note that p is negative of -r (why?) we compute residule as r = Ax-b
    p = -r;
    for its = 1 : max_iter
        h = A * p;
        alpha = delta0 / ( p' * h );
        % we are computing new value of x from initial guess
        x = x + alpha * p;
        % we are computing next residual vector
        r = r + alpha * h;
        %computing new delta value
        delta1 = r' * r;
        %nargout returns the number of output arguments specified in the call
        %to the currently executing function. Use this nargout syntax only in
        %the body of a function
        % nargout will be zero if square of delta1 is less than TOL
        %it will call the below function
        if ( nargout == 0 );
            % this for better output presentation of final solution
            % we can just directly write disp(x)
           s = sprintf ( 'Itearation #%3d \t %10f ', its, x(1) );
           for j = 2 : n
               s = sprintf ( '%s%10f ', s, x(j) );
           end;
           disp ( s );
        end;
        if ( sqrt(delta1) < TOL );
           return
        else
          %updating the values if square of delta1 is less than TOL
           alpha = delta1 / delta0;
           delta0 = delta1;
           p = -r + alpha * p;
        end;
    end;
    %Raise error if number of iteration exceeded
    error ( 'ERROR: maximum number of iterations exceeded' );
end
