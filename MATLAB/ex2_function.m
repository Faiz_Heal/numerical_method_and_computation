%   MANISH KUMAR MEENA
%   2013ME10692
%   Assignment 2


%call ex2_function(f,x0,x1);
function x2 = ex2_function(f,x0,x1)
    %Default values
    max_iter = 100; %maximum number of iteration
    error= 0.00001; %Tolerance
    iter =1;        %initilize iteration
    while iter<max_iter
    x2=x1-(f(x1)*(x1-x0)/(f(x1)-f(x0)));
        if abs(x2-x1)<error;
            fprintf('\nIteration %d: x=%.20f', iter, x1);
            break
        else
            fprintf('\nIteration %d: x0=%.20f x1=%.20f', iter, x0,x1);
            x0=x1;  
            x1=x2;
        iter=iter+1;
        end
    end
    if iter>max_iter
        fprintf('method not converging');
    end
end
