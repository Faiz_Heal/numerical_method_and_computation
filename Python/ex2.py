# -*- coding: utf-8 -*-
"""
Created on Sun Oct 23 17:56:11 2016

@author: manish
"""
#secant method
from __future__ import division
import math
def f(x):
    return math.pow(x,4)-x-10
def secant(f,x0,x1):
    n=1
    max_iter=100
    error=0.001    
    while n<max_iter:
        x2=x1 - ((f(x1))*((x1 - x0)/(f(x1) - f(x0))))
        if abs(x2-x1)<error:
            print 'it converge and the solution is: ', x2
            break
        else:
            print n,'iteration:', x0,x1
            x0=x1
            x1=x2
        n=n+1
    else:
        print 'method not converging'
secant(f,1,2)            
        