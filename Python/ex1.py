# -*- coding: utf-8 -*-
"""
Created on Sun Oct 23 17:03:01 2016

@author: manish
"""

from __future__ import division
import math
def f(x):
    	return math.pow(x,4)-x-10
def df(x):
    return 4*math.pow(x,3)-1
def newton(f,df,x0):
    n=1
    error=0.001
    max_iter=100
    while n<max_iter:
        x1=x0-(f(x0)/df(x0))
        if abs(x1-x0)<error:
            print 'it converge and the solution is: ', x1
            break
        else:
            print n,'iteration', x0
            x0=x1
        n=n+1
    else:
        print 'method not converging'

newton(f,df,2)